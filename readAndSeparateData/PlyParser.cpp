#include "PlyParser.h"

void parsePly(ifstream& myFile, DistributionManager& distManager)
{
	
	distManager.init();
	string linebuffer;

	const char* parsingError = "file can't be parsed";

	enum readingstate{ HEADER, VERTICES, FACES } rstate = HEADER;
	int edgeCounter = 0;
	int remaining_vertices = -1;
	int remaining_faces = -1;

	for (int lineNumber = 0; getline(myFile, linebuffer); lineNumber++)
	{
		if (rstate == HEADER)
		{
			if (linebuffer.find("end_header") != string::npos)
			{
				rstate = VERTICES;
			}
			else if (linebuffer.find("element") != string::npos)
			{
				int pos = 0;
				string vStr = "vertex";
				string fStr = "face";
				if ((pos = linebuffer.find(vStr)) != string::npos)
				{
					remaining_vertices = readElemValue(linebuffer, pos, vStr);

				}
				else if ((pos = linebuffer.find(fStr)) != string::npos)
				{
					remaining_faces = readElemValue(linebuffer, pos, fStr);
				}
			}
		}
		else if (rstate == VERTICES)
		{
			vec3  vertex = vec3();
			string valStr;
			int i = 0;
			stringstream linebufferStream(linebuffer);
			while (getline(linebufferStream, valStr, ' '))
			{
				if (i > 3)throw parsingError;

				if (valStr.size() == 0)continue;
				(vertex)[i] = std::stof(valStr);
				i++;
			}
			distManager.writeVertex(vertex);


			remaining_vertices--;
			if (remaining_vertices == 0)
				rstate = FACES;
		}
		else if (rstate == FACES)
		{
			string valStr;
			uvec3 facetIdc;
			bool firstFigure = true;
			int i = 0;
			stringstream linebufferStream(linebuffer);
			while (getline(linebufferStream, valStr, ' '))
			{
				int vIdx;

				if (i >= 3)
					throw parsingError;
				else if (valStr.size() == 0)
					continue;//more than one space between numbers
				else if (firstFigure)
				{
					vIdx = std::stoi(valStr);
					if (vIdx != 3) // first figure in line only indicates number of vertices in line, should always be 3
						throw parsingError;

					firstFigure = false;
					continue;//more than one space between numbers
				}

				vIdx = std::stoi(valStr);
				facetIdc[i] = vIdx;

				i++;
			}

			distManager.assignFace(facetIdc);

			remaining_faces--;
			if (remaining_faces == 0)
				break;
		}
	}

	distManager.finishDistribution();
};

int readElemValue(string& linebuffer, int& pos, string& elemStr)
{
	int number_elem=-1;
	linebuffer = linebuffer.substr(pos + elemStr.length()+1, linebuffer.length());

	while (linebuffer[0] == ' ')
	{
		linebuffer = linebuffer.substr(1, linebuffer.length());
	}

	int beginNumPos;
	if ((beginNumPos = linebuffer.find_first_of(" ")) != string::npos)
	{
		linebuffer = linebuffer.substr(beginNumPos, linebuffer.size());
	}

	while (linebuffer[0] == ' ')
	{
		linebuffer = linebuffer.substr(1, linebuffer.length());
	}

	number_elem = std::stoi(linebuffer);
	return number_elem;
}