#ifndef DISTRIBUTION_MANAGER
#define DISTRIBUTION_MANAGER
#include "Dependencies.h"
#include "TargetFile.h"
#include <ctime>

class DistributionManager
{
protected:
	int numFiles;
	vector<TargetFile*> targetFiles;

	virtual int decideFile(vec3&, vec3&, vec3&)=NULL;
	virtual void computeMaxMin(){};

vector<vec3> vertices;
	vector<uvec3> faces;
	vector<vector<int>> files_indices;

public:
	DistributionManager(int numFiles){ this->numFiles = numFiles; }

	virtual void writeVertex(glm::vec3&);//writes vertex into a buffer file which is later deleted 
	//Format: x,y,z,p1,p2,...,pn ; pi=position of vertex in target file initially -1


	void assignFace(glm::uvec3&);//param=triangle indices of vectors, asks desiding function in which target file to put
	void assignFaces();
	//the face, looks into the file_indices vector for indices of vertices.
	//writes vertex into file if its pi in buffer file = -1.

	void init();//creates bufferFile and targetFile
	void finishDistribution();//deletes bufferFile

	const vector<TargetFile*>& getTargetFiles(){ return this->targetFiles; }
	
	void setNumFiles(int num){ this->numFiles = num; };
	
};

class DistributionManagerRandom : public DistributionManager
{
protected:
		virtual int decideFile(vec3&, vec3&, vec3&);
public:
	DistributionManagerRandom(int numFiles) :DistributionManager(numFiles){};
};

class DistributionManagerPosition : public DistributionManager
{
private:
		vec3 max = vec3(FLT_MIN, FLT_MIN, FLT_MIN), min = vec3(FLT_MAX, FLT_MAX, FLT_MAX);
		vector<vec3> upperBorders;
		virtual void computeMaxMin();		

protected:
		virtual int decideFile(vec3&, vec3&, vec3&);

public:
		DistributionManagerPosition(int numFiles) :DistributionManager(numFiles){};
		
		virtual void writeVertex(glm::vec3&);
		
		void addUpperBorder(vec3& ub)
		{
			this->upperBorders.push_back(ub);			
		}

		
};

#endif