#ifndef PLY_PARSER
#define PLY_PARSER
#include "Dependencies.h"
#include "DistributionManager.h"
#include <typeinfo>

using namespace std;

int readElemValue(string& linebuffer, int& pos, string& elemStr);

void parsePly(ifstream& myFile, DistributionManager& distManager);

#endif