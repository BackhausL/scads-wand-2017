#include "DistributionManager.h"

void DistributionManager::init()
{
	for (int i = 0; i < numFiles; i++)
	{
		targetFiles.push_back(new TargetFile(i));
	}
}

void DistributionManager::writeVertex(glm::vec3& vertice)
{
	vertices.push_back(vertice);

	vector<int> file_indices = vector<int>();
	for (int i = 0; i < numFiles; i++)
		file_indices.push_back(-1);

	files_indices.push_back(file_indices);
}

void DistributionManager::assignFaces()
{
	for (auto it = this->faces.begin(); it != this->faces.end(); ++it)
	{
		uvec3 oldVidc = *it;
		int fileI;
		vec3 face[3];
		uvec3 newVidc;

		for (int i = 0; i < 3; i++)
		{
			glm::uint& oldVidx = oldVidc[i];
			face[i] = vertices[oldVidx];
		}

		fileI = decideFile(face[0], face[1], face[2]);

		for (int i = 0; i < 3; i++)
		{
			glm::uint& oldVidx = oldVidc[i];
			glm::uint& newVidx = newVidc[i];

			if (oldVidx >= files_indices.size())
			{
				std::cerr << "Index "<<oldVidx<<" for nodes out of bounds." ;
			}

			newVidx = (files_indices[oldVidx])[fileI];

			if (newVidx == -1)
			{
				vec3& vertex = face[i];
				newVidx = targetFiles[fileI]->appendVertex(vertex);
				(files_indices[oldVidx])[fileI] = newVidx;
			}
		}
		targetFiles[fileI]->appendFace(newVidc);
	}	
}

void DistributionManager::assignFace(uvec3&oldVidc)
{
	faces.push_back(oldVidc);
}

void DistributionManager::finishDistribution()
{
	computeMaxMin();
	size_t capacity = ((1.0 / (double)numFiles)+0.2)*(double)vertices.size();
	for (int i = 0; i < targetFiles.size(); i++)
	{		
		targetFiles[i]->set_capacity(capacity);
	}

	assignFaces();
	for (int i = 0; i < numFiles; i++)
	{		
		targetFiles[i]->create();
	}
}

int DistributionManagerRandom::decideFile(vec3&, vec3&, vec3&)
{
	std::srand(std::time(0));
	int buffer =rand() % numFiles;
	return buffer;
}

int DistributionManagerPosition::decideFile(vec3& v1,vec3& v2,vec3& v3) 
{
    vec3 max;
    max.x= (v1.x>v2.x)?v1.x:v2.x;
    max.y= (v1.y>v2.y)?v1.y:v2.y;
    max.z= (v1.z>v2.z)?v1.z:v2.z;
    max.x= (max.x>v3.x)?max.x:v3.x;
    max.y= (max.y>v3.y)?max.y:v3.y;
    max.z= (max.z>v3.z)?max.z:v3.z;
    
    int i=0;
    for(;i<upperBorders.size();i++)
    {
        vec3 &b=upperBorders[i];
        if((max.x<b.x)
            &&(max.y<b.y)
            &&(max.z<b.z))
        {
            return i;
        }
    }
    return i;    
}

void DistributionManagerPosition::writeVertex(glm::vec3& vertice) 
{
	DistributionManager::writeVertex(vertice);

	if (vertice[0] > max[0]) max[0] = vertice[0];
	if (vertice[1] > max[1]) max[1] = vertice[1];
	if (vertice[2] > max[2]) max[2] = vertice[2];

	if (vertice[0] < min[0]) min[0] = vertice[0];
	if (vertice[1] < min[1]) min[1] = vertice[1];
	if (vertice[2] < min[2]) min[2] = vertice[2];
}


void DistributionManagerPosition::computeMaxMin()
{
	vec3 delta = max - min;
	delta /= (numFiles-1);
	vec3 val = min;
	for (int i = 0; i < (numFiles-1); i++)
	{
		val += delta;
		upperBorders.push_back(val);
	}
}

