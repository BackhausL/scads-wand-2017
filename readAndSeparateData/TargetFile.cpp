#include "TargetFile.h"

TargetFile::TargetFile(int idx)
{
	targetFileName << "target_" << idx << ".data";

#ifdef CREATE_TEST
	testFileName << "test_" << idx << ".txt";
#endif 

}

TargetFile::TargetFile(const char* targetFileName)
{
	this->targetFileName << targetFileName;
}

void TargetFile::create()
{	
	this->targetFile = std::fstream(targetFileName.str().c_str(), ios_base::out | ios_base::binary | ios_base::trunc);
	size_t  size = size_t(this->vertices.size());
	auto s = sizeof(glm::uint);
	this->targetFile.write((reinterpret_cast<const char*>(&size)), sizeof(size_t));
	if (vertices.size() != 0)
		this->targetFile.write(reinterpret_cast<const char*>(&vertices[0]), vertices.size()*sizeof(vertices[0]));
	if (faces.size()!=0)
		this->targetFile.write(reinterpret_cast<const char*>(&faces[0]), faces.size()*sizeof(faces[0]));
	this->targetFile.close();

	
#ifdef CREATE_TEST


	
	this->testFile = std::fstream(testFileName.str().c_str(), ios_base::out | ios_base::trunc);
	this->testFile << "vertices\n";
	for (int i = 0; i < vertices.size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			this->testFile << (vertices[i])[j]<<" ";
		}
		this->testFile << "\n";
	}

	this->testFile << "faces\n";
	for (int i = 0; i < faces.size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			this->testFile << (faces[i])[j] << " ";
		}
		this->testFile << "\n";
	}

	this->testFile.close();
#endif 

}
void TargetFile::open()
{
	this->targetFile.open(targetFileName.str().c_str(), ios_base::out | ios_base::binary | ios_base::in);
}


int TargetFile::appendVertex(vec3& v)
{
	static int vCount = -1;
	vertices.push_back(v);
	++vCount;
	return vCount;
}

void TargetFile::appendFace(uvec3& f)
{
	faces.push_back(f);
}

void TargetFile::set_capacity(size_t& capacity)
{
	vertices.reserve(capacity);
	faces.reserve(capacity);
}

void TargetFile::readVertices(vector<vec3>& vertices)
{
	size_t vSize;
	targetFile.seekg(0);
	targetFile.read(reinterpret_cast<char*>(&vSize), sizeof(size_t));
	targetFile.seekg(sizeof(vSize));

	vertices.clear();
	vertices.resize(vSize);

	if (vSize>0)
		this->targetFile.read(reinterpret_cast<char*>(&vertices[0]), vertices.size()*sizeof(vertices));
}

void TargetFile::readFaces(vector<uvec3>& faces)
{
	size_t vSize;
	targetFile.seekg(0);
	targetFile.read(reinterpret_cast<char*>(&vSize), sizeof(size_t));
	

	size_t fSize;
	
		targetFile.seekg(0, std::ios::end);
		auto targetEnd = targetFile.tellg();
		targetFile.seekg(sizeof(vSize) + vSize*sizeof(vec3));
		auto targetFacesBegin = targetFile.tellg();
		fSize = targetEnd - targetFacesBegin;
		fSize /= sizeof(vec3);

	faces.clear();	
	faces.resize(fSize);

	if (vSize>0)
		this->targetFile.read(reinterpret_cast<char*>(&faces[0]), faces.size()*sizeof(faces));
}

void TargetFile::close()
{
	targetFile.close();
}

