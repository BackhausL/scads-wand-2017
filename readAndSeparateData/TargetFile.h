#include "Dependencies.h"

#define CREATE_TEST

#ifndef TARGET_FILE
#define TARGET_FILE

class TargetFile
{
private:
	std::fstream targetFile;
	std::stringstream targetFileName;

#ifdef CREATE_TEST
	std::fstream testFile;
	std::stringstream testFileName;
#endif 

	std::vector<glm::vec3> vertices;
	std::vector<glm::uvec3> faces;

public:
	TargetFile(int idx);
	TargetFile(const char* targetFileName);

	int appendVertex(vec3&);
	void appendFace(uvec3&);

	void set_capacity(size_t&);

	void readVertices(vector<vec3>&);
	void readFaces(vector<uvec3>&);

	void create();
	void open();
	void close();
};

#endif