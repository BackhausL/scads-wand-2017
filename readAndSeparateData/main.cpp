#ifndef MAIN
#include "Dependencies.h"
#include "PlyParser.h"
#include "TargetFile.h"
#include "DistributionManager.h"

using namespace std;

//http://paulbourke.net/dataformats/ply/
int main()
{
	ifstream myFile("data3.ply");
	uint numFiles = 3;
		
	DistributionManagerPosition dr(numFiles);
	parsePly(myFile, dr);//TODO name
	TargetFile* tf=dr.getTargetFiles()[0];
	vector<vec3> vertices;
	tf->open();
	tf->readVertices(vertices);
	
	vector<uvec3> faces;
	tf->readFaces(faces);
	tf->close();


	//system("pause");

	return 0;
}
#endif