#include <stdio.h>
#include "plyloader.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <set>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <string>


bool sortX(glm::vec3 i, glm::vec3 j) { return (i.x < j.x); }

class tri;

class vert {
public:
	vert() {};
	vert(vert* v) {
		this->v = v->v;
		this->id = v->id;
	}

	glm::vec3 v;
	unsigned int id;

};

class tri {
public:
	tri() {};
	/*tri(tri* t) {
		v[0] = new vert(t->v[0]);
		v[1] = new vert(t->v[1]);
		v[2] = new vert(t->v[2]);
	}*/
	tri(vert* verts[3]) {
		for (char i = 0; i < 3; ++i)
			v[i] = verts[i];
	}
	vert* v[3];
};

int main(int argc, const char* argv[])
{
	std::cout << "reading model" << std::endl;

	PLYModel plymodel = PLYModel("eis.ply", false, false);

	std::vector<tri*> tris;
	std::vector<vert*> verts;
	tris.reserve(plymodel.faceCount);
	verts.reserve(plymodel.positions.size());

	std::cout << "init vert and tris" << std::endl;

	unsigned int id = 0;
	for (glm::vec3& v : plymodel.positions) {
		vert* vt = new vert();
		vt->v = glm::vec3(v);
		vt->id = id;
		verts.push_back(vt);
		id++;
	}

	for (auto& i : plymodel.faces) {
		tri* t = new tri();
		tris.push_back(t);

		vert* va = verts.at(i.x);
		vert* vb = verts.at(i.y);
		vert* vc = verts.at(i.z);

		t->v[0] = va;
		t->v[1] = vb;
		t->v[2] = vc;
	}

	//can delete ply model here to save RAM
	plymodel.FreeMemory();

	//left - right  => [2]
	std::set<tri*> tmp_t[2];
	std::set<tri*> to_copy;

	auto width = plymodel.bvWidth;
	auto min = plymodel.min;
	auto max = plymodel.max;
	auto center = plymodel.center;

	std::cout << "choosing left, right or copy" << std::endl;

	//sort tris into left, right, both(copy)
	for (auto t : tris) {
		bool left = false, right = false;
		for (char i = 0; i < 3; ++i) {
			if (t->v[i]->v.x < center.x)
				left = true;
			else right = true;
		}
		if (left && right)
			to_copy.insert(t);
		else if (left) {
			tmp_t[0].insert(t);
		}
		else if (right) {
			tmp_t[1].insert(t);
		}
	}
	tris.clear(); tris.shrink_to_fit();
	verts.clear(); verts.shrink_to_fit();

	std::cout << "copy overlapping data" << std::endl;

	std::set<vert*> copied_verts; // also allocates verts
	std::set<tri*> copied_tris; // also allocates verts

	for (auto t : to_copy) {
		tmp_t[0].insert(t); // just copy tri "and its verts" to left set
		vert* v[3];
		for (char i = 0; i < 3; ++i)
			v[i] = new vert(t->v[i]);
		copied_tris.insert(new tri(v));
		copied_verts.insert(v[0]);
		copied_verts.insert(v[1]);
		copied_verts.insert(v[2]);
	}
	to_copy.clear();

	std::cout << "insert copy into right side" << std::endl;

	//could be set_union but whatever
	tmp_t[1].insert(copied_tris.begin(), copied_tris.end());
	copied_tris.clear();
	copied_verts.clear();

	std::cout << "recalc indices" << std::endl;

	//recalc indices
	std::set<unsigned int> active_ids[2];
	id = 0;
	for (char i = 0; i < 2; ++i) {
		for (tri* t : tmp_t[i]) {
			for (char j = 0; j < 3; ++j) {
				t->v[j]->id = id;
				id++;
				//active_ids[i].insert(t->v[j]->id);
			}
		}
		id = 0;
	}
	size_t active_size[2];
	active_size[0] = active_ids[0].size();
	active_size[1] = active_ids[1].size();
	active_ids[0].clear();
	active_ids[1].clear();

	system("pause");

	std::set<unsigned int> written_ids[2];

	for (char i = 0; i < 2; ++i) {
		std::ofstream file(std::to_string(i) + ".data", std::ios::out | std::ios::binary);
		if (file.is_open())
		{
			std::cout << "write data " + std::to_string(i) << std::endl;
			//write header
			auto v_size = static_cast<unsigned long>(active_ids[i].size());
			file.write(reinterpret_cast<const char*>(&v_size),sizeof(unsigned long));
			auto i_size = static_cast<unsigned long>(tmp_t[i].size());
			file.write(reinterpret_cast<const char*>(&i_size), sizeof(unsigned long));

			//write vertices
			for (tri* t : tmp_t[i]) {
				for (char j = 0; j < 3; ++j) {
					if (written_ids[i].find(t->v[j]->id) == written_ids[i].end()) { //if not in there yet
						file.write(reinterpret_cast<const char*>(glm::value_ptr(t->v[j]->v)), sizeof(glm::vec3));
						written_ids->insert(t->v[j]->id);
					}
				}
			}
			//write tris
			for (tri* t : tmp_t[i]) {
				glm::uvec3 ind;
				for (char j = 0; j < 3; ++j) {
					ind[i] = t->v[j]->id;
				}
				file.write(reinterpret_cast<const char*>(glm::value_ptr(ind)), sizeof(glm::uvec3));
			}

			file.close();
		}
		else std::cerr << "Unable to open file " + std::to_string(i) << std::endl;

		tmp_t[i].clear();
	}

	std::cout << "cleaning memory" << std::endl;

	/*
	//clean stuff
	for (tri* t : tris) {
		delete t;
	}
	tris.clear();
	tris.shrink_to_fit();
	for (vert* v : verts) {
		delete v;
	}
	verts.clear();
	verts.shrink_to_fit();
	//clean up additional data
	for (tri* t : copied_tris) {
		delete t;
	}
	for (vert* v : copied_verts) {
		delete v;
	}
	*/
}