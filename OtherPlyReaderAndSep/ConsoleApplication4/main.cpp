#include <stdio.h>
#include "plyloader.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <set>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <string>
#include <fstream>


bool sortX(glm::vec3 i, glm::vec3 j) { return (i.x < j.x); }

class tri;



class vert {
public:
	vert() {};
	vert(vert* v) {
		this->v = v->v;
		this->id = v->id;
	}

	glm::vec3 v;
	unsigned int id;

};

class tri {
public:
	tri() {};
	/*tri(tri* t) {
		v[0] = new vert(t->v[0]);
		v[1] = new vert(t->v[1]);
		v[2] = new vert(t->v[2]);
	}*/
	tri(vert* verts[3]) {
		for (char i = 0; i < 3; ++i)
			v[i] = verts[i];
	}
	vert* v[3];
};

struct vertptrcomp {
	bool operator() (vert* lhs, vert* rhs) const {
		return lhs->id < rhs->id;
	}
};

int main(int argc, const char* argv[])
{
	std::cout << "reading model" << std::endl;

	PLYModel plymodel = PLYModel("head2.ply", false, false);

	std::vector<tri*> tris;
	std::vector<vert*> verts;
	tris.reserve(plymodel.faceCount);
	verts.reserve(plymodel.positions.size());

	std::cout << "init vert and tris" << std::endl;

	unsigned int id = 0;
	for (glm::vec3& v : plymodel.positions) {
		vert* vt = new vert();
		vt->v = glm::vec3(v);
		vt->id = id;
		verts.push_back(vt);
		id++;
	}

	for (auto& i : plymodel.faces) {
		tri* t = new tri();
		tris.push_back(t);

		vert* va = verts.at(i.x);
		vert* vb = verts.at(i.y);
		vert* vc = verts.at(i.z);

		t->v[0] = va;
		t->v[1] = vb;
		t->v[2] = vc;
	}

	//can delete ply model here to save RAM

	//left - right  => [2]
	std::set<tri*> tmp_t[2];
	std::set<vert*, vertptrcomp> tmp_v[2];
	std::set<tri*> to_copy;

	auto width = plymodel.bvWidth;
	auto min = plymodel.min;
	auto max = plymodel.max;
	auto center = plymodel.center;

	plymodel.FreeMemory();

	std::cout << "choosing left, right or copy" << std::endl;

	//sort tris into left, right, both(copy)
	for (auto t : tris) {
		bool left = false, right = false;
		for (char i = 0; i < 3; ++i) {
			if (t->v[i]->v.x < center.x)
				left = true;
			else right = true;
		}
		if (left && right)
			to_copy.insert(t);
		else if (left) {
			tmp_t[0].insert(t);
			for (char i = 0; i < 3; ++i)
				tmp_v[0].insert(t->v[i]);
		}
		else if (right) {
			tmp_t[1].insert(t);
			for (char i = 0; i < 3; ++i)
				tmp_v[1].insert(t->v[i]);
		}
	}
	tris.clear(); tris.shrink_to_fit();
	verts.clear(); verts.shrink_to_fit();


	std::cout << "recalc indices pass 1 " << std::endl;
	//recalc indices
	unsigned int first_pass_id_end[2];
	id = 0;
	for (char i = 0; i < 2; ++i) {
		for (vert* v : tmp_v[i]) {
			v->id = id;
			id++;
		}
		first_pass_id_end[i] = id;
		id = 0;
	}

	//first pass write
	unsigned long first_pass_t_size[2];
	first_pass_t_size[0] = tmp_t[0].size();
	first_pass_t_size[1] = tmp_t[1].size();
	unsigned long first_pass_v_size[2];
	first_pass_v_size[0] = tmp_v[0].size();
	first_pass_v_size[1] = tmp_v[1].size();

	for (char i = 0; i < 2; ++i) {
		std::ofstream file(std::to_string(i) + ".v", std::ios::out);
		std::ofstream file2(std::to_string(i) + ".t", std::ios::out);
		if (file.is_open() && file2.is_open())
		{
			std::cout << "write data 1st pass " + std::to_string(i) << std::endl;

			//write vertices
			for (vert* v : tmp_v[i]) {
				//file.write(reinterpret_cast<const char*>(glm::value_ptr(v->v)), sizeof(glm::vec3));
				file << "v " << v->v.x << " " << v->v.y << " " << v->v.z << " " << v->id <<  "\n";
			}
			//write tris
			for (tri* t : tmp_t[i]) {
				/*glm::uvec3 ind;
				for (char j = 0; j < 3; ++j) {
					ind[j] = t->v[j]->id;
				}
				file2.write(reinterpret_cast<const char*>(glm::value_ptr(ind)), sizeof(glm::uvec3));*/
				file2 << "f " << t->v[0]->id + 1 << " " << t->v[1]->id + 1 << " " << t->v[2]->id + 1 << "\n";
			}

			file.close();
			file2.close();
		}
		else std::cerr << "Unable to open file " + std::to_string(i) << std::endl;

		tmp_t[i].clear();
		tmp_v[i].clear();
	}

	//system("pause");

	std::cout << "copy overlapping data" << std::endl;

	for (auto t : to_copy) {
		tmp_t[0].insert(t); // just copy tri "and its verts" to left set
		for (char i = 0; i < 3; ++i)
			tmp_v[0].insert(t->v[i]);

		vert* v[3];
		for (char i = 0; i < 3; ++i)
			v[i] = new vert(t->v[i]);
		tmp_t[1].insert(new tri(v));
		for (char i = 0; i < 3; ++i)
			tmp_v[1].insert(v[i]);

	}
	to_copy.clear();

	//system("pause");

	std::cout << "recalc indices pass 2 " << std::endl;
	//recalc indices
	unsigned int second_pass_id_end[2];

	for (char i = 0; i < 2; ++i) {
		id = first_pass_id_end[i];
		for (vert* v : tmp_v[i]) {
			v->id = id;
			id++;
		}
		second_pass_id_end[i] = id;
		id = 0;
	}



unsigned long second_pass_t_size[2];
second_pass_t_size[0] = tmp_t[0].size();
second_pass_t_size[1] = tmp_t[1].size();
unsigned long second_pass_v_size[2];
second_pass_v_size[0] = tmp_v[0].size();
second_pass_v_size[1] = tmp_v[1].size();

for (char i = 0; i < 2; ++i) {
	std::ofstream file(std::to_string(i) + ".v", std::ios::out | std::ios::app);
	std::ofstream file2(std::to_string(i) + ".t", std::ios::out | std::ios::app);
	if (file.is_open() && file2.is_open())
	{
		std::cout << "write data 2nd pass " + std::to_string(i) << std::endl;

		//write vertices
		int z = 0;
		for (vert* v : tmp_v[i]) {
			//file.write(reinterpret_cast<const char*>(glm::value_ptr(v->v)), sizeof(glm::vec3));
			file << "v " << v->v.x << " " << v->v.y << " " << v->v.z << " " <<  v->id <<"\n";
			z++;
		}
		//write tris
		for (tri* t : tmp_t[i]) {
			/*glm::uvec3 ind;
			for (char j = 0; j < 3; ++j) {
				ind[j] = t->v[j]->id;
			}
			file2.write(reinterpret_cast<const char*>(glm::value_ptr(ind)), sizeof(glm::uvec3));*/
			file2 << "f " << t->v[0]->id + 1 << " " << t->v[1]->id + 1 << " " << t->v[2]->id + 1 << "\n";
		}

		file.close();
		file2.close();
	}
	else std::cerr << "Unable to open file " + std::to_string(i) << std::endl;

	tmp_t[i].clear();
	tmp_v[i].clear();
}

//TODO: combine header and 2 passes

//write header
//auto v_size = static_cast<unsigned long>(active_ids[i].size());
//file.write(reinterpret_cast<const char*>(&v_size), sizeof(unsigned long));
//auto i_size = static_cast<unsigned long>(tmp_t[i].size());
//file.write(reinterpret_cast<const char*>(&i_size), sizeof(unsigned long));






std::cout << "cleaning memory" << std::endl;

/*
//clean stuff
for (tri* t : tris) {
	delete t;
}
tris.clear();
tris.shrink_to_fit();
for (vert* v : verts) {
	delete v;
}
verts.clear();
verts.shrink_to_fit();
//clean up additional data
for (tri* t : copied_tris) {
	delete t;
}
for (vert* v : copied_verts) {
	delete v;
}
*/
}