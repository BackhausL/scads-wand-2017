#ifndef TREE
#define TREE
#include <vector>
#include <algorithm>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;

enum CrossingMode{ AFTER, BEFORE, CROSSING };

const float inft = numeric_limits<float>::infinity();

class Face
{
private:
	vector<fvec3*>* vertices;
public:
	Face(vector<fvec3*>* vertices)
	{
		this->vertices = vertices;
	}

	vector<fvec3*>* get_vertices()
	{
		return this->vertices;
	}
	
	fvec3* max();
	fvec3* min();
};

class OktTree_Branch;
class OktTree_Leaf;

class OktTree
{
public:
	static OktTree* create(vector<Face*>* &faces);
	virtual fvec3* max() = NULL;
	virtual fvec3* min() = NULL;
	virtual void separate(fvec3* sepStart, fvec3*sepNormal,
		OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing) = NULL;
};


class OktTree_Branch : public OktTree
{
private:
	OktTree_Branch() = default;
public:
	OktTree *children[8];
	fvec3 mid;

	virtual fvec3* max();
	virtual fvec3* min();
	

	virtual void separate(fvec3* sepStart, fvec3*sepNormal,
		OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing);

	friend OktTree;
};
class OktTree_Leaf : public OktTree //bl�tter m�ssen facetten Sein
{
private:
	OktTree_Leaf() = default;
	Face* face;
public:
	fvec3* vertex;

	virtual fvec3* max();
	virtual fvec3* min();


	virtual void separate(fvec3* sepStart, fvec3*sepNormal,
		OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing);
	friend OktTree;
};
#endif