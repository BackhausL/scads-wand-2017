#ifndef PLY_PARSER
#define PLY_PARSER
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <functional>
#include <array>
//#include "stdafx.h"
#include <vector>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Tree.h"


OktTree* parsePly(ifstream myFile);

#endif