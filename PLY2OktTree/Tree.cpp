#include "Tree.h"


///Helper///////////////////////////////////////////////////////
inline void devideFaceVector(vector<Face*>*& in, vector<Face*>*& out1, vector<Face*>*& out2)
{
	if (in->size() == 0)
	{
		out1 = NULL;
		out2 = NULL;
	}
	else if (in->size() == 1)
	{
		out1 = new vector<Face*>();
		out1->push_back((*in)[0]);
		out2 = NULL;
	}
	else
	{
		int half_size = (*in).size() / 2;
		out1 = new vector<Face*>((*in).begin(), (*in).begin() + half_size);
		out2 = new vector<Face*>((*in).begin() + half_size, (*in).end());
	}
}

inline CrossingMode ray_crosses_cube(fvec3& r, fvec3& n, fvec3& min, fvec3& max)
{
	CrossingMode buffer;
	fvec3 mins[3];
	fvec3 maxs[3];
	mins[0] = min;
	maxs[0] = max;
	mins[1] = fvec3(min.x, max.y, min.z);
	maxs[1] = fvec3(max.x, min.y, max.z);
	mins[2] = fvec3(min.x, max.y, max.z);
	maxs[2] = fvec3(max.x, min.y, min.z);

	for (int i = 0; i < 3; i++)
	{
		float dist1 = dot(n, (mins[i] - r));
		float dist2 = dot(n, (maxs[i] - r));

		if (i == 0)
		{
			if (dist1 >= 0 && dist2 >= 0)
			{
				buffer = BEFORE;
			}
			else if (dist1 < 0 && dist2 < 0)
			{
				buffer = AFTER;
			}
			else
			{
				buffer = CROSSING;
				break;
			}
		}
		else if (buffer == AFTER && (dist1 >= 0 || dist2 >= 0))
		{
			buffer = CROSSING;
			break;
		}
	}
	return buffer;

}


///Face/////////////////////////////////////////////////////////

fvec3* Face::max()
{
	static fvec3* max = NULL;
	if (max == NULL && vertices != NULL)
	{
		max = new fvec3(-inft, -inft, -inft);
		for (vector<fvec3*>::iterator it = this->vertices->begin(); it != this->vertices->end(); ++it)
		{
			if ((*it)->x > max->x)
				max->x = (*it)->x;

			if ((*it)->y > max->y)
				max->y = (*it)->y;

			if ((*it)->z > max->z)
				max->z = (*it)->z;
		}
	}
	return max;
}

fvec3* Face::min()
{
	static fvec3* min = NULL;
	if (min == NULL)
	{
		min = new fvec3(inft, inft, inft);
		for (vector<fvec3*>::iterator it = this->vertices->begin(); it != this->vertices->end(); ++it)
		{
			if ((*it)->x > min->x)
				min->x = (*it)->x;

			if ((*it)->y > min->y)
				min->y = (*it)->y;

			if ((*it)->z > min->z)
				min->z = (*it)->z;
		}
	}
	return min;
}

///OktTree//////////////////////////////////////////////////////

OktTree* OktTree::create(vector<Face*>* &faces)
{
	if (faces == NULL)
	{
		OktTree_Leaf* buffer = new OktTree_Leaf();
		buffer->face = NULL;
		return buffer;
	}
	else if (faces->size()==1)
	{
		OktTree_Leaf* buffer = new OktTree_Leaf();
		buffer->face = (*faces)[0];
		return buffer;
	}	
	else
	{
		OktTree_Branch* buffer = new OktTree_Branch();

		struct sortfu{
			virtual bool operator()(Face* a, Face* b)const = NULL;
		};
		struct :public sortfu{
			virtual bool operator()(Face* a, Face* b)const
			{
				return a->max()->x > b->min()->x;
			}
		} sortx;
		struct :public sortfu{
			virtual bool operator()(Face* a, Face* b)const
			{
				return a->max()->y > b->min()->y;
			}
		} sorty;
		struct :public sortfu{
			virtual bool operator()(Face* a, Face* b)const
			{
				return a->max()->z > b->min()->z;
			}
		} sortz;

		vector<Face*>*facesX[2];
		vector<Face*>*facesXY[4];
		vector<Face*>*facesXYZ[8];

		std::sort(faces->begin(), faces->end(), sortx);
		devideFaceVector(faces, facesX[0], facesX[1]);

		for (int i = 0, j = 0; i < 2; i++, j += 2)
		{
			std::sort(facesX[i]->begin(), facesX[i]->end(), sorty);
			devideFaceVector(facesX[i], facesXY[j], facesXY[j + 1]);
			delete facesX[i];
		}
		for (int i = 0, j = 0; i < 4; i++, j += 2)
		{
			std::sort(facesXY[i]->begin(), facesXY[i]->end(), sortz);
			devideFaceVector(facesXY[i], facesXYZ[j], facesXYZ[j + 1]);
			delete facesXY[i];
		}
		for (int i = 0; i < 8; i++)
		{
			buffer->children[i] = create(facesXYZ[i]);			
			delete facesXYZ[i];
		}

		return buffer;
	}
	
}
//
/////OktTree_Branch///////////////////////////////////////////////

void  OktTree_Branch::separate(fvec3* sepStart, fvec3*sepNormal,
	OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing)
{
	fvec3* max = this->max();
	fvec3* min = this->min();

	fvec3 cubeBound[8];
	cubeBound[0] = *min;
	cubeBound[1] = fvec3(min->x, min->y, max->z);
	cubeBound[2] = fvec3(min->x, max->y, min->z);
	cubeBound[3] = fvec3(min->x, max->y, max->z);
	cubeBound[4] = fvec3(max->x, min->y, min->z);
	cubeBound[5] = fvec3(max->x, min->y, max->z);
	cubeBound[6] = fvec3(max->x, max->y, min->z);
	cubeBound[7] = *max;

	for (int i = 0; i < 8; i++)
	{
		CrossingMode mode = ray_crosses_cube(*sepStart, *sepNormal, cubeBound[0], mid);
		if (mode == BEFORE)
		{
			before.children[i] = this->children[i];
			after.children[i] = NULL;
		}
		else if (mode == AFTER)
		{
			after.children[i] = this->children[i];
			before.children[i] = NULL;
		}
		else if (mode == CROSSING)
		{
			OktTree_Branch* beforeBranch = new OktTree_Branch();
			OktTree_Branch* afterBranch = new OktTree_Branch();
			children[i]->separate(sepStart, sepNormal, before, after, crossing);
			before.children[i] = beforeBranch;
			after.children[i] = afterBranch;
		}
	}

}

fvec3* OktTree_Branch::max()
{
	static fvec3* max = NULL;
	if (max == NULL)
	{
		max = new fvec3(-inft, -inft, -inft);
		for (int i = 0; i < 8;i++)
		{
			fvec3* chMax=children[i]->max();
			if (chMax->x > max->x)
				max->x = chMax->x;

			if (chMax->y > max->y)
				max->y = chMax->y;

			if (chMax->z > max->z)
				max->z = chMax->z;
		}	
	}

	return max;
}

fvec3* OktTree_Branch::min()
{
	static fvec3* min = NULL;
	if (min == NULL)
	{
		min = new fvec3(-inft, -inft, -inft);
		for (int i = 0; i < 8; i++)
		{
			fvec3* chMin = children[i]->min();
			if (chMin->x < min->x)
				min->x = chMin->x;

			if (chMin->y < min->y)
				min->y = chMin->y;

			if (chMin->z < min->z)
				min->z = chMin->z;
		}		
	}
	return min;
}

/////OktTree_Leaf/////////////////////////////////////////////////
//
void  OktTree_Leaf::separate(fvec3* sepStart, fvec3*sepNormal,
	OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing)
{
	crossing.push_back(this);
}
//
fvec3* OktTree_Leaf::max()
{
	static fvec3* max = NULL;
	if (max == NULL)
	{
		max = this->max();
	}
	return max;
}

fvec3* OktTree_Leaf::min()
{
	static fvec3* min = NULL;
	if (min == NULL)
	{
		min = this->min();
	}
	return min;
}

//////Not needed///////////////////////////////

//enum CrossingMode{AFTER,BEFORE,CROSSING};
//
//class Face
//{
//public:
//	vector<fvec3> vertices;
//	fvec3 max(); 
//	fvec3 min();
//};
//
//class OktTree
//{	
//public:
//	fvec3 max, min;
//	
//	OktTree* create(vector<Face*>* &faces);
//	virtual void separate(fvec3* sepStart, fvec3*sepNormal,
//		OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing)=NULL;
//};
//
//class OktTree_Branch : public OktTree
//{
//private:
//	OktTree_Branch() = default;
//public:
//	
//	OktTree *children[8];
//	fvec3 mid;
//	void init(vector<Face*> &faces);
//	//vector<OktTree_Branch> separate(glm::fvec3* sepStart, glm::fvec3*sepNormal,
//	//	float dist = 0, float count = 0);
//	
//	virtual void separate(fvec3* sepStart, fvec3*sepNormal,
//		OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing);
//
//	friend OktTree;
//};
//class OktTree_Leaf : public OktTree //bl�tter m�ssen facetten Sein
//{
//private:
//	OktTree_Leaf() = default;
//	Face* face;
//public:
//	glm::fvec3* vertex;
//	void init(Face* &face);
//	virtual void separate(fvec3* sepStart, fvec3*sepNormal,
//		OktTree_Branch &before, OktTree_Branch &after, vector<OktTree_Leaf*> &crossing);
//	friend OktTree;
//};
//