#include "PlyParser.h"

using namespace std;

int readElemValue(string& linebuffer, int& pos, string& elemStr)
{
	int number_elem;
	linebuffer = linebuffer.substr(pos + elemStr.length(), linebuffer.length());

	for (int i = 0; linebuffer[0] == ' '; i++)
	{
		linebuffer = linebuffer.substr(1, linebuffer.length());
	}

	int endNumPos;
	if (endNumPos = linebuffer.find(" ") != string::npos)
	{
		linebuffer = linebuffer.substr(0, endNumPos);
	}

	number_elem = std::stoi(linebuffer);
	return number_elem;
}

struct PlyParserBuffer
{
	vector<fvec3*> vertices;
	vector<vector<int>*> vertIdc;
	OktTree* getFaces();
};

OktTree* PlyParserBuffer::getFaces()
{
	vector<Face*>*faces = new vector < Face* > ();
	for (vector<vector<int>*>::iterator it = vertIdc.begin(); it != vertIdc.end(); ++it)
	{
		
		vector<fvec3*> *faceVertices = new vector<fvec3*>();
		

		for (vector<int>::iterator it1 = (*it)->begin(); it1 != (*it)->end(); it1++)
		{
			int idx = *it1;
			faceVertices->push_back(vertices[idx]);
		}
		
		Face *face = new Face(faceVertices);
		faces->push_back(face);
	}
	OktTree* buffer = OktTree::create(faces);
	return buffer;
}

OktTree* parsePly(ifstream myFile)
{
	PlyParserBuffer buffer;
	string linebuffer;

	char* parsingError = "file can't be parsed";


	enum readingstate{ HEADER, VERTICES, FACES }rstate = HEADER;
	int edgeCounter = 0;
	int remaining_vertices = -1;
	int remaining_faces = -1;

	while (getline(myFile, linebuffer))
	{
		if (rstate = HEADER)
		{
			if (linebuffer.find("end_header") != string::npos)
			{
				rstate = VERTICES;
			}
			else if (linebuffer.find("element") != string::npos)
			{
				int pos = 0;
				string vStr = "vertex";
				string fStr = "face";
				readElemValue(linebuffer, pos, vStr);
				if ((pos = linebuffer.find(vStr)) != string::npos)
				{
					remaining_vertices = readElemValue(linebuffer, pos, vStr);

				}
				else if (linebuffer.find(fStr) != string::npos)
				{
					remaining_faces = readElemValue(linebuffer, pos, fStr);
				}
			}
		}
		else if (rstate = VERTICES)
		{
			glm::fvec3 * vertex=new glm::fvec3();
			string valStr;
			int i = 0;
			while (getline(stringstream(linebuffer), valStr, ' '))
			{
				if (valStr.size() == 0)continue;
				(*vertex)[i] = std::stof(valStr);
				i++;
			}
			if (i != 3)throw parsingError;

			buffer.vertices.push_back(vertex);

			remaining_vertices--;
			if (remaining_vertices == 0)
				rstate = FACES;
		}
		else if (rstate = FACES)
		{
			string valStr;
			std::vector<int> * facetIdc = new std::vector<int>();
			int i = 0;
			while (getline(stringstream(linebuffer), valStr, ' '))
			{
				if (valStr.size() == 0)continue;
				if (i == 0)continue; //i==0 gibt nur Anzahl der vertices f�r Facet an
				int vIdx = std::stoi(valStr);
				facetIdc->push_back(vIdx);
				i++;
			}

			buffer.vertIdc.push_back(facetIdc);
		}
		
			
	}

	OktTree* retBuffer = buffer.getFaces();
	return retBuffer;

}