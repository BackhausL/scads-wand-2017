#include "GLDataContainer.h"
#include <iostream>
#include "Utils.h"

GLDataContainer::GLDataContainer() : draw_type(arrays)
{

}

GLDataContainer::~GLDataContainer()
{
    //clean up
    glDeleteBuffers(1, &v_buf);
    glDeleteBuffers(1, &n_buf);
    if(draw_type == indexed)
        glDeleteBuffers(1, &ind_buf);
    glDeleteVertexArrays(1, &vao); 
}

void GLDataContainer::move_copy_vertices(std::vector<glm::vec3>& vertices)
{
    this->vertices = std::move(vertices);
}
void GLDataContainer::move_copy_indices(std::vector<glm::uvec3>& indices)
{
    this->indices = std::move(indices);
    draw_type = indexed;
}
void GLDataContainer::move_copy_normals(std::vector<glm::vec3>& normals)
{
    this->normals = std::move(normals);
}

void GLDataContainer::create_buffers()
{
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &v_buf);
    if(!normals.empty())
        glGenBuffers(1, &n_buf);
    bind();
  
    
    glBindBuffer(GL_ARRAY_BUFFER, v_buf);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3),
        vertices.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    if(!normals.empty()){
    
    glBindBuffer(GL_ARRAY_BUFFER, n_buf);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(glm::vec3),
        normals.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    if(draw_type == indexed){
    glGenBuffers(1, &ind_buf);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ind_buf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(glm::uvec3),
        indices.data(), GL_STATIC_DRAW);
    }
    unbind();
}

void GLDataContainer::bind()
{
    glBindVertexArray(vao);
}
void GLDataContainer::unbind()
{
    glBindVertexArray(0);
}

void GLDataContainer::draw()
{
    switch(draw_type){
        case arrays:
            drawArrays();
            break;
        case indexed:
            drawElements();
            break;
        case lines:
            drawLines();
            break;
    }
}
void GLDataContainer::drawArrays()
{
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}
void GLDataContainer::drawElements()
{
    glDrawElements(GL_TRIANGLES, indices.size()*3, GL_UNSIGNED_INT, nullptr);
}
void GLDataContainer::drawLines()
{
    glDrawElements(GL_LINES, indices.size()*3, GL_UNSIGNED_INT, nullptr);
}

std::vector<glm::vec3> GLDataContainer::get_vertices()
{
    return vertices;
}
std::vector<glm::uvec3> GLDataContainer::get_indices()
{
    return indices;
}
std::vector<glm::vec3> GLDataContainer::get_normals()
{
    return normals;
}

void GLDataContainer::init_triforce_data()
{
  vertices.push_back(glm::vec3(-8.0f, 1.5f, 3.0f));
  vertices.push_back(glm::vec3(-9.5f, -1.5f, 3.0f));
  vertices.push_back(glm::vec3(-6.5f, -1.5f, 3.0f));
  vertices.push_back(glm::vec3(-8.0f+3.0f, 1.5f, 3.0f));
  vertices.push_back(glm::vec3(-9.5f+3.0f, -1.5f, 3.0f));
  vertices.push_back(glm::vec3(-6.5f+3.0f, -1.5f, 3.0f));
  vertices.push_back(glm::vec3(-8.0f+1.5f, 1.5f+3.0f, 3.0f));
  vertices.push_back(glm::vec3(-9.5f+1.5f, -1.5f+3.0f, 3.0f));
  vertices.push_back(glm::vec3(-6.5f+1.5f, -1.5f+3.0f, 3.0f));
  
  indices.push_back(glm::uvec3(0,1,2));
  indices.push_back(glm::uvec3(3,4,5));
  indices.push_back(glm::uvec3(6,7,8));
  
//   glm::vec3 n = glm::cross(vertices[0], vertices[1]);
//   normals.push_back(n);
//   normals.push_back(n);
//   normals.push_back(n);
//   glm::vec3 n1 = glm::cross(vertices[3], vertices[4]);
//   normals.push_back(n1);
//   normals.push_back(n1);
//   normals.push_back(n1);
//   glm::vec3 n2 = glm::cross(vertices[6], vertices[7]);
//   normals.push_back(n2);
//   normals.push_back(n2);
//   normals.push_back(n2);

  draw_type = indexed;
}