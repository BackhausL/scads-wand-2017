#ifndef UTILS_H
#define UTILS_H
#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include "glm/glm.hpp"
#include <omp.h>

//obj stuff
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

namespace Utils{

    //dont care about about ODR, just make everything inline
    inline std::string read_file_as_text(const std::string& path){
    std::ifstream file(path);
    if(!file.is_open()){
        std::cerr << "could not find given path" << std::endl;
        return "";
    }
        
    file.seekg(0, std::ifstream::end);
    size_t length = file.tellg();
    std::vector<char> content;
    content.resize(length);
    file.seekg(0, std::ifstream::beg);
    file.read(content.data(), length);
    //std::cout << std::string(content.data(), length);
    return std::string(content.data(), length);
    }

    inline std::vector<glm::vec3> calculate_normals(const std::vector<glm::vec3>& vertices,
                                             const std::vector<glm::uvec3>& triangle_indices){
    	unsigned int tri_num = static_cast<unsigned int>(triangle_indices.size());

    	std::vector<glm::vec3> normals;
    	std::vector<glm::vec3> tri_normals;
		normals.resize(vertices.size());
		tri_normals.resize(tri_num);

		

		#pragma omp parallel for
		for (int t = 0; t < static_cast<int>(tri_num); t++) {
			tri_normals.at(t) = glm::cross(vertices.at(triangle_indices.at(t).y) - vertices.at(triangle_indices.at(t).x),
										   vertices.at(triangle_indices.at(t).z) - vertices.at(triangle_indices.at(t).x));
		//std::cout << tri_normals.at(t).x << " " << tri_normals.at(t).y << " " << tri_normals.at(t).z << std::endl;
		}

		//build lookup
		std::vector<std::vector<unsigned int>> tri_of_vertex(vertices.size());
		for (std::vector<unsigned int>& stv : tri_of_vertex) {
			stv.reserve(6); //average tri net
		}

		//for every tri write it's index into vertex->tri assoziation
		for (unsigned int i = 0; i < tri_num; i++) {
			tri_of_vertex.at(triangle_indices.at(i).x).push_back(i);
			tri_of_vertex.at(triangle_indices.at(i).y).push_back(i);
			tri_of_vertex.at(triangle_indices.at(i).z).push_back(i);
		}

		//add all tri normals
		#pragma omp parallel for
		for (int t = 0; t < static_cast<int>(vertices.size()); t++) {
			std::vector<unsigned int> tris = tri_of_vertex.at(t);
			for (unsigned int i : tris) {
				normals.at(t) += tri_normals.at(i);
			}
			//normalize afterwards, so length of tri affects normal
			normals.at(t) = glm::normalize(normals.at(t));
		}

        return normals;
	}

	inline void load_obj(const char* filename, std::vector<glm::vec3>& vertices,
	 std::vector<glm::uvec3>& indices){
		vertices.clear();
		indices.clear();
		//printf ( "Loading Objects %s ... \n",filename);
		FILE* fn;
		if(filename==NULL)		return ;
		if((char)filename[0]==0)	return ;
		if ((fn = fopen(filename, "rb")) == NULL)
		{
			printf ( "File %s not found!\n" ,filename );
			return;
		}
		char line[1000];
		memset ( line,0,1000 );
		int vertex_cnt = 0;
		while(fgets( line, 1000, fn ) != NULL)
		{
			glm::vec3 v;
			if ( line[0] == 'v' )
			{
				if ( line[1] == ' ' )
				if(sscanf(line,"v %f %f %f",
					&v.x,	&v.y,	&v.z)==3)
				{
					vertices.push_back(v);
				}
			}
			int integers[9];
			if ( line[0] == 'f' )
			{
				bool tri_ok = false;

				if(sscanf(line,"f %d %d %d",
					&integers[0],&integers[1],&integers[2])==3)
				{
					tri_ok = true;
				}else
				if(sscanf(line,"f %d// %d// %d//",
					&integers[0],&integers[1],&integers[2])==3)
				{
					tri_ok = true;
				}else
				if(sscanf(line,"f %d//%d %d//%d %d//%d",
					&integers[0],&integers[3],
					&integers[1],&integers[4],
					&integers[2],&integers[5])==6)
				{
					tri_ok = true;
				}else
				if(sscanf(line,"f %d/%d/%d %d/%d/%d %d/%d/%d",
					&integers[0],&integers[6],&integers[3],
					&integers[1],&integers[7],&integers[4],
					&integers[2],&integers[8],&integers[5])==9)
				{
					tri_ok = true;
				}
				else
				{
					printf("unrecognized sequence\n");
					printf("%s\n",line);
					while(1);
				}
				if ( tri_ok )
				{
					glm::uvec3 t;
					t.x = integers[0]-1-vertex_cnt;
					t.y = integers[1]-1-vertex_cnt;
					t.z = integers[2]-1-vertex_cnt;
					indices.push_back(t);
				}
			}
		}
		fclose(fn);
		printf("load_obj: vertices = %lu, triangles = %lu\n", vertices.size(), indices.size()/3 );
	} // load_obj()

	inline void make_bbox(const std::vector<glm::vec3> in, std::vector<glm::vec3>& vert,
						  std::vector<glm::uvec3>& ind)
	{
		glm::vec3 min(FLT_MAX), max(FLT_MIN);

		for(auto i : in){
			min.x = min.x > i.x ? i.x : min.x;
			min.y = min.y > i.y ? i.y : min.y;
			min.z = min.z > i.z ? i.z : min.z;
			max.x = max.x < i.x ? i.x : max.x;
			max.y = max.y < i.y ? i.y : max.y;
			max.z = max.z < i.z ? i.z : max.z;
		}

		vert.resize(8);
		ind.resize(8);

		vert[0] = glm::vec3(min.x, min.y, min.z);
		vert[1] = glm::vec3(min.x, min.y, max.z);
		vert[2] = glm::vec3(max.x, min.y, max.z);
		vert[3] = glm::vec3(max.x, min.y, min.z);

		vert[4] = glm::vec3(max.x, max.y, min.z);
		vert[5] = glm::vec3(min.x, max.y, min.z);
		vert[6] = glm::vec3(min.x, max.y, max.z);
		vert[7] = glm::vec3(max.x, max.y, max.z);

		ind[0] = glm::uvec3(0,1,1);
		ind[1] = glm::uvec3(2,2,3);
		ind[2] = glm::uvec3(3,0,4);
		ind[3] = glm::uvec3(5,5,6);
		ind[4] = glm::uvec3(6,7,7);
		ind[5] = glm::uvec3(4,0,5);
		ind[6] = glm::uvec3(1,6,2);
		ind[7] = glm::uvec3(7,3,4);
	}
}

#endif // UTILS_H