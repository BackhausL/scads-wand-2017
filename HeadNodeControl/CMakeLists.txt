cmake_minimum_required(VERSION 2.6)
project(headnodecontrol)

set(libraries)

find_package(PkgConfig REQUIRED)
set( ENV{PKG_CONFIG_PATH} "$ENV{PKG_CONFIG_PATH}:${LOCAL_LIB_PATH}/pkgconfig" )

find_package(PkgConfig REQUIRED)
pkg_search_module(GLFW REQUIRED glfw3)
include_directories(${GLFW_INCLUDE_DIRS})
list(APPEND libraries ${GLFW_LIBRARIES})

find_package(OpenGL REQUIRED)
list(APPEND libraries ${OPENGL_gl_LIBRARY})
include_directories(${OPENGL_INCLUDE_DIR})

pkg_check_modules(PC_ZeroMQ QUIET zmq)
#find_path(ZeroMQ_INCLUDE_DIR NAMES zmq.hpp PATHS ${PC_ZeroMQ_INCLUDE_DIRS})
find_library(ZeroMQ_LIBRARY NAMES zmq PATHS ${PC_ZeroMQ_LIBRARY_DIRS})
list(APPEND libraries ${ZeroMQ_LIBRARY})

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

add_definitions(-Wall -pedantic -ansi -std=c++11 -DGLM_FORCE_RADIANS -fopenmp)

set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")

add_executable(headnodecontrol zmqserver.cpp ViewInteractor.cpp box.cpp main.cpp window.cpp autoInit.cpp Shader.cpp GLDataContainer.cpp)

target_link_libraries(headnodecontrol -lX11 -lXrandr -lXinerama -lXi -lXxf86vm -lXcursor -lGL -lpthread -ldl -lglfw -lGLEW ${libraries})

install(TARGETS headnodecontrol RUNTIME DESTINATION bin)
