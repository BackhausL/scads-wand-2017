#include "box.h"

box::box() : x1(-1.0f), y1(-1.0f), z1(-1.0f),
    x2(1.0f), y2(1.0f), z2(1.0f) {
}

box::box(scalar x1, scalar y1, scalar z1, scalar x2, scalar y2, scalar z2)
    : x1(x1), y1(y1), z1(z1), x2(x2), y2(y2), z2(z2) {
}

box::box(const box& other) : x1(other.x1), y1(other.y1), z1(other.z1),
    x2(other.x2), y2(other.y2), z2(other.z2) {
}

box::~box() {
}

box& box::operator=(const box& other) {
  x1 = other.x1;
  y1 = other.y1;
  z1 = other.z1;
  x2 = other.x2;
  y2 = other.y2;
  z2 = other.z2;
  return *this;
}
