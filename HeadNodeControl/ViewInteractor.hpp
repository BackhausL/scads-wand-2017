#ifndef VIEW_INTERACTOR_H
#define VIEW_INTERACTOR_H


#include <iostream>
//#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

/* Viewing control with orbital rotation, zoom and panning.
   To update the status of mouse buttons, position and keys
   use the appropriate update methods. You can get the viewing
   matrix by calling getTransformation. You can directly use
   the result as view matrix in OpenGL.
   Example usage:
	ViewInteractor interactor;
	...	
	void SomeWindow::mousePressEvent(...) {
		interactor.updateButtons(isLeft, isRight, isMiddle);
	}

	void SomeWindow::mouseReleaseEvent(...) {
		interactor.updateButtons(isLeft, isRight, isMiddle);
	}

	void SomeWindow::mouseMoveEvent(...) {
		interactor.updatePosition(
		            static_cast<float>(mouseX) / static_cast<float>(windowWidth),
		            static_cast<float>(mouseY) / static_cast<float>(windowHeight);
	}


	void SomeWindow::wheelEvent(...) {
		interactor.updateWheel(isWheelScrolledUp);
	}

	void SomeWindow::keyPressEvent(...) {
		interactor.updateModifiers(isShift, isAlt, isCtrl);
		interactor.updateKeys(isSpace);
	}

	void SomeWindow::keyReleaseEvent(...)
	{
		interactor.updateModifiers(isShift, isAlt, isCtrl);
		interactor.updateKeys(isSpace);
	} 

	void SomeWindow::render(...) {
		...
		glm::mat4 viewMatrix = interactor.getTransformation();
		...
	}
*/


class ViewInteractor {

	public:
		ViewInteractor();

		// Convenience method to update all modifiers
		void updateModifiers(bool shift, bool alt, bool ctrl);
		// Convenience method to update all buttons 
		void updateButtons(bool left, bool right, bool middle);
		// Update status of the space key. Needed to reset the view
		void updateKeys(bool space);

		// Update mouse position, normalized from 0 to 1
        void updatePosition(float pos_x, float pos_y);
		// Update mouse wheel. Call whenever the wheel is scrolled.
		void updateWheel(bool dir_up);

		// Update status of left mouse button. Not needed if you use 
		// the convenience function above.
		void updateLeftButton(bool left);
		// Update status of right mouse button. Not needed if you use 
		// the convenience function above.
		void updateRightButton(bool right);
		// Update status of middle mouse button (wheel press). 
		// Not needed if you use the convenience function above.
		void updateMiddleButton(bool middle);


		// Update status of shift modifier. Not needed if you use 
		// the convenience function above.
        void updateShift(bool pressed);
		// Update status of alt modifier. Not needed if you use 
		// the convenience function above.
        void updateAlt(bool pressed);
		// Update status of ctrl modifier. Not needed if you use 
		// the convenience function above.
        void updateCtrl(bool pressed);

		// Get the view matrix.
		glm::mat4 getTransformation();

	private:
		// flags that define the current state
		bool shift, alt, ctrl;
		bool left, right, middle;
		bool space;
		bool wheel_dir_up, wheel_active;
		bool position_valid;
        float pos_x, pos_y;
        float delta_x, delta_y;
        float old_pos_x, old_pos_y;
		float sensitivity;

		glm::vec3 eye, focus, up_dir;

		glm::vec3 space_eye, space_focus, space_up_dir;

		void updateTransformation();
		void rotateSpherical(glm::vec3& target, const glm::vec3& center, glm::vec3& up, bool swap_y = false);
		void rotateUpDir(glm::vec3 &target, const glm::vec3 &axis);
		void moveImagePlane(glm::vec3& target, glm::vec3& center, const glm::vec3& up);
		void moveTowardsCenter(glm::vec3& target, glm::vec3& center, float step);
		void reassignFocusFromScene();
		void resetView();
		bool updateSpaceTransition();
};

#endif