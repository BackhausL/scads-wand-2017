#include <stdio.h>
#include "plyloader.h"
#include <iostream>
#include <glm\glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <set>
#include <cmath>
#include <fstream> 
#include <string>
#include <cstdio>
#include <errno.h>
#include <chrono>


struct vert {
	glm::vec3 pos;
	//glm::vec3 nor;
	size_t id;
};

struct vert_compare {
	bool operator() (const vert& lhs, const vert& rhs) const {
		if (lhs.pos.x < rhs.pos.x) return true;
		if (rhs.pos.x < lhs.pos.x) return false;
		if (lhs.pos.y < rhs.pos.y) return true;
		if (rhs.pos.y < lhs.pos.y) return false;
		return lhs.pos.z < rhs.pos.z;
	}
};

std::string getHeader(size_t vertexNumber, size_t faceNumber) {
	std::string header =
		"ply \n"
		"format binary_little_endian 1.0\n"
		"element vertex " + std::to_string(vertexNumber) + "\n"
		"property float32 x\n"
		"property float32 y\n"
		"property float32 z\n"
		"property float32 nx\n"
		"property float32 ny\n"
		"property float32 nz\n"
		"element face " + std::to_string(faceNumber) + "\n"
		"property list uchar int32 vertex_index\n" //normally would use uint32, but meshlab doesnt like it
		"end_header\n";

		return header;
}

std::vector<glm::vec3> calculate_normals(const std::vector<glm::vec3>& vertices,
	const std::vector<glm::ivec3>& triangle_indices) {

	std::vector<glm::vec3> normals;
	normals.resize(vertices.size());

#pragma omp parallel for
	for (int t = 0; t < static_cast<int>(triangle_indices.size()); t++) {
		int ind[3];
		for (int i = 0; i < 3; ++i)
			ind[i] = triangle_indices.at(t)[i];
		glm::vec3 n = glm::cross(vertices.at(ind[1]) - vertices.at(ind[0]),
								 vertices.at(ind[2]) - vertices.at(ind[0]));
		for (int i = 0; i < 3; ++i)
			normals.at(ind[i]) += n;
	}

	// normalize
#pragma omp parallel for
	for (int t = 0; t < static_cast<int>(normals.size()); t++) {
		normals.at(t) = glm::normalize(normals.at(t));
	}
	return normals;
}

int main(int argc, const char* argv[])
{

	if (argc < 2) {
		std::cerr << "no input file given" << std::endl;
		return - 1;
	}

	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

	PLYModel plymodel = PLYModel(argv[1], false, false);

	std::cout << "calculating normals" << std::endl;
	std::vector<glm::vec3> normals = calculate_normals(plymodel.positions, plymodel.faces);

	double xSplit = 0;
	for (auto& f : plymodel.positions) {
		xSplit += static_cast<double>(f.x);
	}
	xSplit /= plymodel.positions.size();
	float cutLine = static_cast<float>(xSplit);

	std::set<vert, vert_compare> lookUpLeft;
	std::set<vert, vert_compare> lookUpRight;

	//vertexnumber and facenumber are written in a second pass
	

	std::ofstream leftFile("left.ply", std::ios::out | std::ios::binary);
	std::ofstream rightFile("right.ply", std::ios::out | std::ios::binary);

	if (!(leftFile.is_open() && rightFile.is_open())) 
		std::cerr << "couldn't open left and right output files" << std::endl;

	std::ofstream leftVertexFile("leftVertex.dat", std::ios::out | std::ios::binary);
	std::ofstream rightVertexFile("rightVertex.dat", std::ios::out | std::ios::binary);
	std::ofstream leftFaceFile("leftFace.dat", std::ios::out | std::ios::binary);
	std::ofstream rightFaceFile("rightFace.dat", std::ios::out | std::ios::binary);

	if (!(leftVertexFile.is_open() && rightVertexFile.is_open() && leftFaceFile.is_open() && rightFaceFile.is_open())) 
		std::cerr << "couldn't open temporary left & right data files" << std::endl;

	uint32_t posInVec = 0;
	int onePercent = plymodel.faceCount / 100;
	int percentage = 0;
	std::chrono::high_resolution_clock::time_point timeP = std::chrono::high_resolution_clock::now();

	size_t vertexLeft = 0;
	size_t vertexRight = 0;
	size_t faceLeft = 0;
	size_t faceRight = 0;
	unsigned char writeFaceCount = 3;
	long long mod = 0;

	std::cout << "start splitting: " << std::endl;

	for (auto face : plymodel.faces) {
		int onLeftSide = 0;
		for (int i = 0; i < 3; ++i) {
			if (plymodel.positions.at(face[i]).x <= cutLine) 
				++onLeftSide;
		}

		if ((++mod) % 2 == 0) { //right
			glm::ivec3 newFace; // to view in meshlab, just use ivec3
			for (int i = 0; i < 3; ++i) {
				std::pair<std::set<vert>::iterator, bool> ret = lookUpRight.insert(vert{ plymodel.positions.at(face[i]), vertexRight });
				if (ret.second) {
					newFace[i] = (*ret.first).id;
					rightVertexFile.write(reinterpret_cast<char*>(glm::value_ptr(plymodel.positions.at(face[i]))), sizeof(glm::vec3));
					rightVertexFile.write(reinterpret_cast<char*>(glm::value_ptr(normals.at(face[i]))), sizeof(glm::vec3));
					++vertexRight;
				}
				else {
					newFace[i] = (*ret.first).id;
				}
			}
			rightFaceFile.write(reinterpret_cast<char*>(&writeFaceCount), sizeof(unsigned char));
			rightFaceFile.write(reinterpret_cast<char*>(glm::value_ptr(newFace)), sizeof(glm::uvec3));
			++faceRight;

		} else { // left
			glm::ivec3 newFace2;
			for (int i = 0; i < 3; ++i) {
				std::pair<std::set<vert>::iterator, bool> ret = lookUpLeft.insert(vert{ plymodel.positions.at(face[i]),vertexLeft });
				if (ret.second) {
					newFace2[i] = (*ret.first).id;
					leftVertexFile.write(reinterpret_cast<char*>(glm::value_ptr(plymodel.positions.at(face[i]))), sizeof(glm::vec3));
					leftVertexFile.write(reinterpret_cast<char*>(glm::value_ptr(normals.at(face[i]))), sizeof(glm::vec3));
					++vertexLeft;
				}
				else {
					newFace2[i] = (*ret.first).id;
				}
			}
			leftFaceFile.write(reinterpret_cast<char*>(&writeFaceCount), sizeof(unsigned char));
			leftFaceFile.write(reinterpret_cast<char*>(glm::value_ptr(newFace2)), sizeof(glm::uvec3));
			++faceLeft;
		}

		++posInVec;
		if (posInVec % onePercent == 0) {
			percentage++;
			auto now = std::chrono::high_resolution_clock::now();
			auto timeForOnePercent = std::chrono::duration_cast<std::chrono::milliseconds>(now - timeP).count();
			timeP = now;
			int percentRemain = 100 - percentage;
			std::cout << "\r  " << static_cast<int>((static_cast<float>(posInVec) / plymodel.faceCount) * 100.0f) << " %    "
				<< ((percentRemain * timeForOnePercent) / 1000) << " s";
		}
	}

	std::cout << "\nsplitting done" << std::endl;
	std::cout << "clearing temporary memory" << std::endl;
	//clean up
	plymodel.positions.clear(); plymodel.positions.shrink_to_fit();
	plymodel.faces.clear(); plymodel.faces.shrink_to_fit();
	normals.clear(); normals.shrink_to_fit();
	lookUpLeft.clear();
	lookUpRight.clear();
	//close handles
	leftVertexFile.close();
	rightVertexFile.close();
	leftFaceFile.close();
	rightFaceFile.close();

	leftFile << getHeader(vertexLeft, faceLeft);
	rightFile << getHeader(vertexRight, faceRight);

	std::cout << "merging left file" << std::endl;

	std::ifstream mergeLeftVertex("leftVertex.dat", std::ios::binary);
	std::ifstream mergeLeftFace("leftFace.dat", std::ios::binary);
	if (mergeLeftVertex.is_open() && mergeLeftFace.is_open()) {
		leftFile << mergeLeftVertex.rdbuf();
		leftFile << mergeLeftFace.rdbuf();
	}
	else std::cerr << "couldn't open left merge files" << std::endl;

	std::cout << "merging right file" << std::endl;

	std::ifstream mergeRightVertex("rightVertex.dat", std::ios::binary);
	std::ifstream mergeRightFace("rightFace.dat", std::ios::binary);
	if (mergeRightVertex.is_open() && mergeRightFace.is_open()) {
		rightFile << mergeRightVertex.rdbuf();
		rightFile << mergeRightFace.rdbuf();
	}
	else std::cerr << "couldn't open right merge files" << std::endl;

	std::cout << "delete temp files: ";

	if(std::remove("leftVertex.dat") != 0) std::cerr << strerror(errno) << std::endl;
	std::remove("leftFace.dat");
	std::remove("rightVertex.dat");
	std::remove("rightFace.dat");

	leftFile.close();
	rightFile.close();

	auto end = std::chrono::high_resolution_clock::now();
	auto totalTime = std::chrono::duration_cast<std::chrono::seconds>(end - start).count();

	std::cout << "done! total time: " << totalTime << " s" << std::endl;

	

}