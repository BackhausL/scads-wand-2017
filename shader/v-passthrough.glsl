#version 330 core

uniform mat4 mv_mat;
uniform mat4 p_mat;

layout (location = 0) in vec3 vert_pos;

void main()
{
	vec4 pos_world = vec4(vert_pos, 1.0);
	vec4 pos = p_mat * mv_mat * pos_world;
    gl_Position = pos;
}
