#version 330 core

uniform mat4 mv_mat;
uniform mat4 p_mat;
uniform vec3 color;

in vec3 frag_normal;
in vec3 frag_cam_dir;

out vec4 final_color;

void main()
{

	vec3 l = normalize(vec3(0.0,0.0,1.0));
	vec3 n = normalize(frag_normal);
	vec3 c = normalize(frag_cam_dir);

	float n_dot_l = dot(n,l);
	vec3 r = normalize(2.0 * vec3(n_dot_l) * n - l);

	vec3 col = color;

	#define LIGHT_AMBIENT 0.1
	#define LIGHT_DIFFUSE 0.5
	#define LIGHT_SPECULAR 0.2
	#define LIGHT_EXPONENT 4.0

	float diffuse = LIGHT_DIFFUSE * max(n_dot_l, 0.0);
	vec3 specular = LIGHT_SPECULAR * vec3(pow(max(dot(r,c), 0.0),LIGHT_EXPONENT));
	vec3 lit_col = (LIGHT_AMBIENT + diffuse) * col + specular;

    final_color = vec4(lit_col, 1.0);
} 
