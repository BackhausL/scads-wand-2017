#version 330 core

uniform mat4 mv_mat;
uniform mat4 p_mat;

layout (location = 0) in vec3 vert_pos;
layout (location = 1) in vec3 vert_normal;

out vec3 frag_normal;
out vec3 frag_cam_dir;

void main()
{
	vec4 pos_world = vec4(vert_pos, 1.0);
	vec3 cam_world = inverse(mv_mat)[3].xyz;
	frag_cam_dir = cam_world - pos_world.xyz / pos_world.w;
	frag_normal = (transpose(inverse(mv_mat)) * vec4(vert_normal, 1.0)).xyz;
 
	vec4 pos = p_mat * mv_mat * pos_world;
    gl_Position = pos;
}
