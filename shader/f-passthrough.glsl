#version 330 core

uniform mat4 mv_mat;
uniform mat4 p_mat;
uniform vec3 color;

out vec4 final_color;

void main()
{
    final_color = vec4(color,1.0);
} 
