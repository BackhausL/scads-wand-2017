#include "renderer.h"
#include <GL/gl.h>
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include "Utils.h"
#include "mpiUtil.h"
#include <cstdio>
#include <iostream>
#include <chrono>
#include <ctime>
#include <fstream>

#include "plyloader.h"

projectionUtil renderer::proj;
viewSettings renderer::vs;
Shader renderer::blinn_phong_shader;
Shader renderer::pass_shader;
GLDataContainer renderer::data;
GLDataContainer renderer::cake;
GLDataContainer renderer::boubox;
int renderer::pass = 0;

std::chrono::time_point<std::chrono::system_clock> startT, endT;
std::chrono::duration<double> elapsed_seconds;
std::chrono::time_point<std::chrono::system_clock> start1, start2, start3, end1, end2, end3;
std::chrono::duration<double> elapsed_seconds1, elapsed_seconds2, elapsed_seconds3;


renderer::renderer()/* : bbox(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f)*/ {
    //init shader
    if (!renderer::blinn_phong_shader.load_compile("shader/v-blinn-phong.glsl", "shader/f-blinn-phong.glsl"))
        return;
    if (!renderer::blinn_phong_shader.link())
        return;
    renderer::blinn_phong_shader.add_uniform("mv_mat");
    renderer::blinn_phong_shader.add_uniform("p_mat");
    renderer::blinn_phong_shader.add_uniform("color");

    if (!renderer::pass_shader.load_compile("shader/v-passthrough.glsl", "shader/f-passthrough.glsl"))
        return;
    if (!renderer::pass_shader.link())
        return;
    renderer::pass_shader.add_uniform("mv_mat");
    renderer::pass_shader.add_uniform("p_mat");
    renderer::pass_shader.add_uniform("color");

    std::string path = "";
    if(mpiUtil::rank() == 0) path = "left.ply";
    else if(mpiUtil::rank() == 1) path = "right.ply";
    else path = "right.ply";
    std::cout << mpiUtil::rank() << " loading model" << std::endl;
    PLYModel plymodel = PLYModel(path.c_str(), true, false);

    std::cout << mpiUtil::rank() << " has loaded files" << std::endl;

    //auto cn = Utils::calculate_normals(plymodel.positions, plymodel.faces);

    renderer::data.move_copy_vertices(plymodel.positions);
    renderer::data.move_copy_indices(plymodel.faces);
    renderer::data.move_copy_normals(plymodel.normals);
    renderer::data.create_buffers();

    renderer::data.get_vertices().clear();
    renderer::data.get_vertices().shrink_to_fit();
    renderer::data.get_indices().clear();
    renderer::data.get_indices().shrink_to_fit();

    std::vector <glm::vec3> bv;
    std::vector <glm::ivec3> iv; //dont use this
    Utils::make_bbox(renderer::data.get_vertices(), bv, iv);
    renderer::boubox.move_copy_vertices(bv);
    renderer::boubox.move_copy_indices(iv);
    renderer::boubox.create_buffers();
}

renderer::~renderer() {
    // intentionally empty
}

void renderer::initRender(const projectionUtil &projc, const viewSettings &vsc) {
    proj = projc;
    vs = vsc;
}

void renderer::doRender() {
startT = std::chrono::system_clock::now();
    auto pj = proj.next_projection();

    ::glMatrixMode(GL_PROJECTION);
    ::glLoadMatrixf(pj);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //start rendering

    glm::vec3 color;
    switch (mpiUtil::rank()) {
        case 0:
            color = glm::vec3(1, 0, 0);
            break;
        case 1:
            color = glm::vec3(0, 1, 0);
            break;
        case 2:
            color = glm::vec3(0, 0, 1);
            break;
        default:
            color = glm::vec3(1, 1, 1);
            break;
    }

    blinn_phong_shader.use();
    glUniformMatrix4fv(blinn_phong_shader.get_uniform_location("mv_mat"),
                       1, GL_FALSE, vs.viewMatrix); //this creates std matrix from scratch
    glUniformMatrix4fv(blinn_phong_shader.get_uniform_location("p_mat"),
                       1, GL_FALSE, pj);

    glUniform3fv(blinn_phong_shader.get_uniform_location("color"), 1,
                 glm::value_ptr(color));

    data.bind();
    data.draw();
    data.unbind();

    blinn_phong_shader.unuse();

    pass_shader.use();
    glUniformMatrix4fv(pass_shader.get_uniform_location("mv_mat"),
                       1, GL_FALSE, vs.viewMatrix); //this creates std matrix from scratch
    glUniformMatrix4fv(pass_shader.get_uniform_location("p_mat"),
                       1, GL_FALSE, pj);
    glUniform3fv(pass_shader.get_uniform_location("color"), 1,
                 glm::value_ptr(color));

    boubox.bind();
    boubox.drawLines();
    boubox.unbind();

    pass_shader.unuse();


endT = std::chrono::system_clock::now();
elapsed_seconds = endT-startT;
//std::cout << "elapsed DrawCallBack Rendertime on Node " << mpiUtil::rank() << " : " << elapsed_seconds.count() << "s\n";
    //end rendering
}

