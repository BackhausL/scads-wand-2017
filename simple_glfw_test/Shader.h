#ifndef SHADER_H
#define SHADER_H
#pragma once

#include <string>
#include <map>

#include "GL/glew.h"
#include "GLFW/glfw3.h"

class Shader {
public:
    Shader();

    Shader(const std::string &vertex_source, const std::string &fragment_source);

    ~Shader();

    void uninit();

    bool load_compile(const std::string &vertex_source, const std::string &fragment_source);

    bool link();

    void use();

    void unuse();

    void add_uniform(std::string uniform);

    GLuint get_uniform_location(std::string uniform);

    GLuint get_program();

    GLuint get_vertex_shader();

    GLuint get_fragment_shader();

private:
    GLuint program;
    GLuint vertex_shader;
    GLuint fragment_shader;

    std::map <std::string, GLuint> uniform_locations;
};

#endif // SHADER_H