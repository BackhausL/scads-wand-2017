#include <iostream>
#include <chrono>
#include <ctime>
#include <thread>
#include "mpiUtil.h"
#include "windowFullscreen.h"
#include <zmq.hpp>
#include "renderer.h"
#include "viewSettings.h"
#include "projectionUtil.h"
#include <glm/gtc/matrix_transform.hpp>
#include "zmqclient.h"
#include "IceT.h"
#include "IceTGL.h"
#include "IceTMPI.h"

#include "Utils.h"


static IceTContext icetContext;

///
/// Application main entry point
///
int main(int argc, char **argv) {
    // initialize libraries
    if (!mpiUtil::inst().init(&argc, &argv)) return -1;
    if (!windowFullscreen::init()) {
        mpiUtil::inst().deinit();
        return -2;
    }

    // create fullscreen window for opengl rendering
    windowFullscreen window;

    // initialize application
    viewSettings view;
    projectionUtil proj;
    renderer rnd;
    zmqclient *client = nullptr;

    // IceT & MPI
    int rank, numProc;
    IceTCommunicator icetComm;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numProc);

    icetComm = icetCreateMPICommunicator(MPI_COMM_WORLD);
    icetContext = icetCreateContext(icetComm);
    icetDestroyMPICommunicator(icetComm);
    icetGLInitialize();
    icetGLDrawCallback(renderer::doRender);

    icetResetTiles();
    icetAddTile(0, 0, 1920, 2160, 0);
    icetAddTile(1920, 0, 1920, 2160, 1);
    //icetAddTile(3840, 0, 1920, 2160, 2);

    icetCompositeMode(ICET_COMPOSITE_MODE_Z_BUFFER);
    icetStrategy(ICET_STRATEGY_REDUCE);
    icetSingleImageStrategy(ICET_SINGLE_IMAGE_STRATEGY_TREE);
    icetGLSetReadBuffer(GL_FRONT);
    std::cout << "icet error:" << icetGetError() << std::endl;

    glClearColor(0,0,0,0);

    // Wall-Setup (Projektionsmatrix, Fullscreen)
    view.isAlive = true;
    proj.setViewSize(window.getWidth(), window.getHeight());
    proj.setupTile( // TODO: this should be a config file
            1920 * numProc, 2160, // full wall size
            1920 * mpiUtil::rank(), 0,
            1920, 2160
    );
    proj.updateProjMatSpinning(numProc);
    proj.set_spinner(numProc - 1);

    if (mpiUtil::rank() == 0) {
        // first rank 0 establishes connection with the head node on 'mary'

        client = new zmqclient("mary", 23452);
        //to locally test, replace mary by your pc name
        //client = new zmqclient("xyanide-NB", 23452);

        std::cout << mpiUtil::rank() << " ZMQ Connecting to head node ..." << std::endl;
        if (!client->initPing()) {
            std::cerr << mpiUtil::rank() << " ZMQ init ping handshake failed" << std::endl;
            view.isAlive = false;
        }


        //
        // TODO: implement
        // here we initialize the bounding box
        //
        view.boundingBox[0] = -3.0f;
        view.boundingBox[1] = view.boundingBox[2] = -1.0f;
        view.boundingBox[3] = 3.0f;
        view.boundingBox[4] = view.boundingBox[5] = 1.0f;


        if (view.isAlive) {
            client->sendBoundingBox(view.boundingBox);
            if (!client->updateViewSettings(view)) {
                view.isAlive = false;
            }
        }

    }

    // wait for MPI world to finish initialization
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&view, sizeof(viewSettings), MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);


//std::chrono::time_point<std::chrono::system_clock> startT, endT;
//std::chrono::duration<double> elapsed_seconds;
//std::chrono::time_point<std::chrono::system_clock> start1, start2, start3, end1, end2, end3;
//std::chrono::duration<double> elapsed_seconds1, elapsed_seconds2, elapsed_seconds3;
double time_render, time_bufferRead, time_bufferWrite, time_compress, time_blend, time_collect, time_draw, time_composite;

    // application main event loop
    while (view.isAlive) {
        window.doEvents();

//startT = std::chrono::system_clock::now();

        if (window.isValid()) {

            window.beginRender();

            // performing the actual rendering
            renderer::initRender(proj, view);

            std::vector <glm::vec3> bv;
            std::vector <glm::uvec3> iv; //dont use this

            icetBoundingBoxf(-1, 1, -1, 1, -1, 1);
	    
            glEnable(GL_DEPTH_TEST);
    	    //glDepthFunc(GL_LESS);

//start1 = std::chrono::system_clock::now();	
            icetGLDrawFrame();
//end1 = std::chrono::system_clock::now();
icetGetDoublev(ICET_RENDER_TIME, 	&time_render);
icetGetDoublev(ICET_BUFFER_READ_TIME, 	&time_bufferRead);
icetGetDoublev(ICET_BUFFER_WRITE_TIME, 	&time_bufferWrite);
icetGetDoublev(ICET_COMPRESS_TIME, 	&time_compress);
icetGetDoublev(ICET_BLEND_TIME, 	&time_blend);
icetGetDoublev(ICET_COLLECT_TIME, 	&time_collect);
icetGetDoublev(ICET_TOTAL_DRAW_TIME, 	&time_draw);
icetGetDoublev(ICET_COMPOSITE_TIME, 	&time_composite);


	    glDisable(GL_DEPTH_TEST);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

            // synchronizing the final buffer swaps as good as possible.
            ::glFlush();
            MPI_Barrier(MPI_COMM_WORLD);

            window.endRender();

        }
//endT = std::chrono::system_clock::now();
//elapsed_seconds = endT-startT;
//elapsed_seconds1 = end1-start1;
//std::cout << "elapsed time on Node " << mpiUtil::rank() << " : " << elapsed_seconds.count() << "s, amount of time not in IceT : " << (elapsed_seconds.count() - elapsed_seconds1.count()) << "s\n";
std::cout.precision(3);
std::cout << "Ren: " << time_render << " \tBRd: " << time_bufferRead << " \tBWrt:" << time_bufferRead << " \tcompr:" << time_compress << " \tblnd:" << time_blend << " \tCllct:" << time_collect << " \tdrw:" << time_draw << " comp:" << time_composite << "s\n";

        if (view.isAlive && (mpiUtil::rank() == 0)) {
            // udpate viewSettings by communicating with the host
            if (client != nullptr) {
                if (!client->updateViewSettings(view)) {
                    view.isAlive = false;
                }
            }
        }

        // synchronizing view settings in the whole MPI world
        MPI_Bcast(&view, sizeof(viewSettings), MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);
    }

    // cleanup environment
    if (client != nullptr) {
        delete client;
        client = nullptr;
    }
    window.close();
    icetDestroyContext(icetContext);
    windowFullscreen::deinit();
    mpiUtil::inst().deinit();
    return 0;
}
