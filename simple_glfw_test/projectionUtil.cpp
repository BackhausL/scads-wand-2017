#include "projectionUtil.h"
#include <glm/gtc/matrix_transform.hpp>

namespace {
    static const float epsilon = 0.0001f;
}

projectionUtil::projectionUtil()
        : proj(), fovyRad(glm::radians(60.0f)), nearClip(0.1f), farClip(1000.0f), aspectRatio(1.0f), proj_spinner(0) {

}

projectionUtil::~projectionUtil() {

}

void projectionUtil::set(float fovyRad, float nearClip, float farClip) {
    if ((std::abs(this->fovyRad - fovyRad) < epsilon)
        || (std::abs(this->nearClip - nearClip) < epsilon)
        || (std::abs(this->farClip - farClip) < epsilon)) {
        this->fovyRad = fovyRad;
        this->nearClip = nearClip;
        this->farClip = farClip;
        updateProjMat();
    }
}

void projectionUtil::setAspect(float ar) {
    if (std::abs(aspectRatio - ar) < epsilon) {
        aspectRatio = ar;
        updateProjMat();
    }
}

void projectionUtil::updateProjMat() {
    //proj = glm::perspective(fovyRad, aspectRatio, nearClip, farClip);

    float ar = aspectRatio;
    float left = 0.0f;
    float right = 1.0f;
    float top = 0.0f;
    float bottom = 1.0f;

    if ((fullWidth > 0) && (fullHeight > 0) && (tileWidth > 0) && (tileHeight > 0)) {
        left = static_cast<float>(tileX) / static_cast<float>(fullWidth);
        right = static_cast<float>(tileX + tileWidth) / static_cast<float>(fullWidth);
        top = static_cast<float>(tileY) / static_cast<float>(fullHeight);
        bottom = static_cast<float>(tileY + tileHeight) / static_cast<float>(fullHeight);
        ar = static_cast<float>(fullWidth) / static_cast<float>(fullHeight);
    }

    float ymax = nearClip * std::tan(fovyRad * 0.5f);
    float xmax = ymax * ar;

    proj = glm::frustum(
            glm::mix(-xmax, xmax, left),
            glm::mix(-xmax, xmax, right),
            glm::mix(-ymax, ymax, top),
            glm::mix(-ymax, ymax, bottom),
            nearClip, farClip);
}

void projectionUtil::updateProjMatSpinning(int numProc) {

    float ar = aspectRatio;
    float left = 0.0f;
    float right = 1.0f;
    float top = 0.0f;
    float bottom = 1.0f;

    //proj_spinner = (proj_spinner + 1) % num_projs;
    auto temp_tile_x = tileX;
    auto temp_tile_y = tileY;

    for (int i = 0; i < numProc; ++i) {

        //spin to next tile
        temp_tile_x = (temp_tile_x + i * tileWidth) % fullWidth;

        if ((fullWidth > 0) && (fullHeight > 0) && (tileWidth > 0) && (tileHeight > 0)) {
            left = static_cast<float>(temp_tile_x) / static_cast<float>(fullWidth);
            right = static_cast<float>(temp_tile_x + tileWidth) / static_cast<float>(fullWidth);
            top = static_cast<float>(temp_tile_y) / static_cast<float>(fullHeight);
            bottom = static_cast<float>(temp_tile_y + tileHeight) / static_cast<float>(fullHeight);
            ar = static_cast<float>(fullWidth) / static_cast<float>(fullHeight);
        }

        float ymax = nearClip * std::tan(fovyRad * 0.5f);
        float xmax = ymax * ar;

        auto temp_proj = glm::frustum(
                glm::mix(-xmax, xmax, left),
                glm::mix(-xmax, xmax, right),
                glm::mix(-ymax, ymax, top),
                glm::mix(-ymax, ymax, bottom),
                nearClip, farClip);

        projections.push_back(temp_proj);
    }
}


