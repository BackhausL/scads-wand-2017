#include "zmqclient.h"
#include <string>
#include <iostream>

zmqclient::zmqclient(const char *host, int port) : context(1), socket(nullptr) {
    socket = new zmq::socket_t(context, ZMQ_REQ);
    std::string addr = "tcp://";
    addr = addr + host + ":" + std::to_string(port);
    socket->connect(addr.c_str());

    socket->setsockopt(ZMQ_SNDTIMEO, 100); // 100ms Timeout
    socket->setsockopt(ZMQ_RCVTIMEO, 900);
    socket->setsockopt(ZMQ_LINGER, 100); // 100ms more for rank zero on shutdown

}

zmqclient::~zmqclient() {
    if (socket != nullptr) {
        socket->close();
        delete socket;
        socket = nullptr;
    }
}

bool zmqclient::initPing() {
    const unsigned char magicByte = 57;
    zmq::message_t ping(2);
    ping.data<unsigned char>()[0] = 3; // ping
    ping.data<unsigned char>()[1] = magicByte;

    if (!socket->send(ping)) return false;
    zmq::message_t reply;
    if (!socket->recv(&reply)) return false;

    if (reply.size() != 2) return false;
    if (reply.data<unsigned char>()[0] != 3) return false;
    if (reply.data<unsigned char>()[1] != magicByte) return false;

    return true;
}

void zmqclient::sendBoundingBox(const float *data) {
    static_assert(sizeof(float) == 4, "You got strange floats man");
    zmq::message_t msg(1 + 6 * 4);
    msg.data<unsigned char>()[0] = 1; // send bbox
    ::memcpy(msg.data<unsigned char>() + 1, data, 4 * 6);
    socket->send(msg);
    zmq::message_t reply;
    socket->recv(&reply);
}

bool zmqclient::updateViewSettings(viewSettings &view) {
    zmq::message_t msg(1);
    msg.data<unsigned char>()[0] = 2; // ask for view settings
    socket->send(msg);
    zmq::message_t reply;
    socket->recv(&reply);
    if (reply.size() != 1 + 4 * 4 * 4 + 3 * 4) return false;

    const float *mvp = reinterpret_cast<const float *>(reply.data<const unsigned char>() + 1);
    view.viewMatrix[0] = mvp[0];
    view.viewMatrix[1] = mvp[1];
    view.viewMatrix[2] = mvp[2];
    view.viewMatrix[3] = mvp[3];
    view.viewMatrix[4] = mvp[4];
    view.viewMatrix[5] = mvp[5];
    view.viewMatrix[6] = mvp[6];
    view.viewMatrix[7] = mvp[7];
    view.viewMatrix[8] = mvp[8];
    view.viewMatrix[9] = mvp[9];
    view.viewMatrix[10] = mvp[10];
    view.viewMatrix[11] = mvp[11];
    view.viewMatrix[12] = mvp[12];
    view.viewMatrix[13] = mvp[13];
    view.viewMatrix[14] = mvp[14];
    view.viewMatrix[15] = mvp[15];
    view.projFovyRad = mvp[16];
    view.nearClip = mvp[17];
    view.farClip = mvp[18];

    return true;
}
