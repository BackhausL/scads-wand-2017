#ifndef ZMQCLIENT_H
#define ZMQCLIENT_H
#pragma once

#include <zmq.hpp>
#include "viewSettings.h"

class zmqclient {
public:
    zmqclient(const char *host, int port);

    ~zmqclient();

    bool initPing();

    void sendBoundingBox(const float *data);

    bool updateViewSettings(viewSettings &view);

private:
    zmq::context_t context;
    zmq::socket_t *socket;
};

#endif // ZMQCLIENT_H
