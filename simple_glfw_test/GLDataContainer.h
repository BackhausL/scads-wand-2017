#ifndef GLDATACONTAINER_H
#define GLDATACONTAINER_H
#pragma once

#include <string>
#include <vector>

#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"

class GLDataContainer {

    enum draw_type {
        arrays = 0, indexed, lines
    };

public:
    GLDataContainer();

    ~GLDataContainer();

    //not tested!
    void move_copy_vertices(std::vector <glm::vec3> &vertices);

    void move_copy_indices(std::vector <glm::ivec3> &indices);

    void move_copy_normals(std::vector <glm::vec3> &normals);

    void create_buffers();

    void bind();

    void unbind();

    void draw();

    void drawArrays();

    void drawElements();

    void drawLines();

    std::vector <glm::vec3> get_vertices();

    std::vector <glm::ivec3> get_indices();

    std::vector <glm::vec3> get_normals();

    void init_triforce_data();

private:
    std::vector <glm::vec3> vertices;
    std::vector <glm::ivec3> indices;
    std::vector <glm::vec3> normals;

    GLuint vao;
    GLuint v_buf;
    GLuint n_buf;
    GLuint ind_buf;

    draw_type draw_type;
};

#endif // GLDATACONTAINER_H
