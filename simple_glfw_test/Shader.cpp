#include "Shader.h"
#include "Utils.h"
#include <iostream>
#include <utility>

Shader::Shader() : program(-1), vertex_shader(-1), fragment_shader(-1) {

}

Shader::Shader(const std::string &vertex_source, const std::string &fragment_source) {
    if (!load_compile(vertex_source, fragment_source)) return;
    if (!link()) return;
}

Shader::~Shader() {
    //remember that using a local copy aka: shader = Shader() calls destructor
    uninit();
}

void Shader::uninit() {
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    glDeleteProgram(program);
}

bool Shader::load_compile(const std::string &vertex_source, const std::string &fragment_source) {
    std::string vs_src = Utils::read_file_as_text(vertex_source);
    const char *vs_csrc = vs_src.c_str();
    if (vs_src.empty()) {
        std::cerr << "Could not read Vertex Shader Source" << std::endl;
        return false;
    }
    std::string fs_src = Utils::read_file_as_text(fragment_source);
    const char *fs_csrc = fs_src.c_str();
    if (fs_src.empty()) {
        std::cerr << "Could not read Fragment Shader Source" << std::endl;
        return false;
    }

    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vs_csrc, NULL);
    glCompileShader(vertex_shader);

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fs_csrc, NULL);
    glCompileShader(fragment_shader);

    GLint vs_comp;
    GLint fs_comp;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vs_comp);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fs_comp);

    if (vs_comp != GL_TRUE) {
        std::cerr << "vertex shader failed to compile" << std::endl;
        return false;
    }
    if (fs_comp != GL_TRUE) {
        std::cerr << "fragment shader failed to compile" << std::endl;
        return false;
    }


    return true;
}


bool Shader::link() {
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    GLint link_stat = GL_FALSE;
    ::glGetProgramiv(program, GL_LINK_STATUS, &link_stat);

    if (link_stat == GL_FALSE) {
        GLint log_len = 0;
        ::glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_len);
        std::vector<char> log(log_len);
        GLsizei log_len2;
        ::glGetProgramInfoLog(program, log_len, &log_len2, log.data());
        std::string err = std::string().assign(log.data(), log_len2);
        std::cerr << err << std::endl;
        return false;
    }
    return true;
}

void Shader::use() {
    glUseProgram(program);
}

void Shader::unuse() {
    glUseProgram(0);
}

void Shader::add_uniform(std::string uniform) {
    GLuint loc = glGetUniformLocation(program, uniform.c_str());
    uniform_locations.insert(std::make_pair(uniform, loc));
}

GLuint Shader::get_uniform_location(std::string uniform) {
    return uniform_locations.find(uniform)->second;
}

GLuint Shader::get_program() {
    return program;
}

GLuint Shader::get_vertex_shader() {
    return vertex_shader;
}

GLuint Shader::get_fragment_shader() {
    return fragment_shader;
}