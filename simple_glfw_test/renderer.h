#ifndef RENDERER_H
#define RENDERER_H

#include "viewSettings.h"
#include "box.h"
#include "projectionUtil.h"

#include "Shader.h"
#include "GLDataContainer.h"

class renderer {
public:
    renderer();

    ~renderer();

    static void initRender(const projectionUtil &proj, const viewSettings &vs);

    static void doRender();


    static void test();

    static projectionUtil proj;
    static viewSettings vs;

    //if compiler can handle c++14 -> unique_ptr
    static Shader blinn_phong_shader;
    static Shader pass_shader;
    static GLDataContainer data;
    static GLDataContainer cake;
    static GLDataContainer boubox;
    static int pass;

private:

};

#endif // RENDERER_H
